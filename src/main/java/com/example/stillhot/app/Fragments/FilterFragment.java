package com.example.stillhot.app.Fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.AppController;

/**
 * Created by dev on 15.10.2015.
 */
public class FilterFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.filter_fragment, null);
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.FilterFragment";
        MainActivity.setVisibilityForLogo();
        RadioGroup rg_sort = (RadioGroup) root.findViewById(R.id.rg_sort);
        int id = R.id.radioButton;
        switch (AppController.sort){
            case 0:
                id = R.id.radioButton;
                break;
            case 1:
                id = R.id.radioButton2;
                break;
        }
        RadioButton radioButton = (RadioButton) root.findViewById(id);
        radioButton.setChecked(true);

        int id_min = 0;
        switch (AppController.minSum){
            case 500:
                id_min = R.id.radioButton3;
                break;
            case 1000:
                id_min = R.id.radioButton4;
                break;
            case 1500:
                id_min = R.id.radioButton5;
                break;
        }
        if(id_min != 0) {
            radioButton = (RadioButton) root.findViewById(id_min);
            radioButton.setChecked(true);
        }
        rg_sort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case(R.id.radioButton):
                        AppController.sort = 0;
                        break;
                    case(R.id.radioButton2):
                        AppController.sort = 1;
                        break;
                }
            }
        });
        RadioGroup rg_min = (RadioGroup) root.findViewById(R.id.rg_min);
        rg_min.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case(R.id.radioButton3):
                        AppController.minSum = 500;
                        break;
                    case(R.id.radioButton4):
                        AppController.minSum = 1000;
                        break;
                    case(R.id.radioButton5):
                        AppController.minSum = 1500;
                        break;
                }
            }
        });

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "HelveticaNeueCyr-Light.otf");
        RadioButton rb_name = (RadioButton)root.findViewById(R.id.radioButton);
        rb_name.setTypeface(font);
        RadioButton rb_ratio = (RadioButton)root.findViewById(R.id.radioButton2);
        rb_ratio.setTypeface(font);
        RadioButton rb_3 = (RadioButton)root.findViewById(R.id.radioButton3);
        rb_3.setTypeface(font);
        RadioButton rb_4 = (RadioButton)root.findViewById(R.id.radioButton4);
        rb_4.setTypeface(font);
        RadioButton rb_5 = (RadioButton)root.findViewById(R.id.radioButton5);
        rb_5.setTypeface(font);
        return root;
    }
}
