package com.example.stillhot.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.Addresses;
import com.example.stillhot.app.back.Connection;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.back.maps.RestAddress;
import com.example.stillhot.app.back.maps.RestObjSingle;
import com.example.stillhot.app.back.maps.Restaurant;
import com.example.stillhot.app.pojo.BaseFragment;
import com.example.stillhot.app.pojo.ObservableScrollView;
import com.example.stillhot.app.pojo.ObservableScrollViewCallbacks;
import com.example.stillhot.app.pojo.ScrollUtils;
import com.google.gson.Gson;

/**
 * Created by Admin on 24.10.2015.
 */
public class RestInfoFragment extends BaseFragment {
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scrollview, container, false);
        final LinearLayout root = (LinearLayout) view.findViewById(R.id.root_layout);
        final ViewGroup content = (ViewGroup) inflater.inflate(R.layout.rest_info_fragment, null);
        root.addView(content);

        final ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Fragment parentActivity = getParentFragment();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_SCROLL_Y)) {
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
            }

            // TouchInterceptionViewGroup should be a parent view other than ViewPager.
            // This is a workaround for the issue #117:
            // https://github.com/ksoichiro/Android-ObservableScrollView/issues/117
            scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.getView().findViewById(R.id.root));
            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }
        final Gson gson = new Gson();
        City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        Restaurant restaurant = gson.fromJson(Preferences.get().getString(Preferences.RESTAURANT), Restaurant.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.restinfo
                + "city=" + city.getId()
                + "&rest=" + restaurant.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        RestObjSingle restObj = gson.fromJson(response, RestObjSingle.class);
                        if (restObj != null) {
                            Restaurant restInfo = restObj.getRestaurant();
                            if (restInfo != null) {
                                TextView tv_time = (TextView) content.findViewById(R.id.textView9);
                                tv_time.setText(restInfo.getApp_time());
                                TextView tv_desc = (TextView) content.findViewById(R.id.textView11);
                                tv_desc.setText(restInfo.getApp_desc());
                                TextView tv_kitchen = (TextView) content.findViewById(R.id.textView13_);
                                tv_kitchen.setText(restInfo.getApp_content());
                                TextView tv_address = (TextView) content.findViewById(R.id.textView13);
                                RestAddress[] restAddresses = restObj.getRest_addr();
                                String str = "";
                                if(restAddresses != null){
                                    for (int i = 0; i < restAddresses.length - 1; i++) {
                                        str = str + restAddresses[i].getAddress() + "\n";
                                    }
                                    str = str + restAddresses[restAddresses.length - 1].getAddress();
                                }
                                tv_address.setText(str);
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, RestInfoFragment.class.getName());
        return view;
    }
}