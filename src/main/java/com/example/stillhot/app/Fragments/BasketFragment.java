package com.example.stillhot.app.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.stillhot.app.*;
import com.example.stillhot.app.back.DatabaseHelper;
import com.example.stillhot.app.back.Preferences;
import com.example.stillhot.app.back.maps.Dish;
import com.example.stillhot.app.back.maps.DishObj;
import com.example.stillhot.app.back.maps.Restaurant;
import com.example.stillhot.app.ui.MyTextViewLight;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Admin on 17.10.2015.
 */
public class BasketFragment extends Fragment {
    ListAdapterDishBasket adapter;
    ListView list;
    static TextView sum, total, min_sum_to;
    RelativeLayout totalLayout, minSumLayout, rlOrederLeft;
    int priceMin;
    MyTextViewLight textViewLight;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.basketscreen, null);
        MainActivity.currentFragment = "com.example.stillhot.app.Fragments.BasketFragment";
        MainActivity.setVisibilityForBasket();
        totalLayout = (RelativeLayout) root.findViewById(R.id.relativeLayout4);
        minSumLayout = (RelativeLayout) root.findViewById(R.id.relativeLayout3);
        rlOrederLeft = (RelativeLayout) root.findViewById(R.id.rl_order_left);
        textViewLight = (MyTextViewLight) root.findViewById(R.id.textView65_);
        textViewLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        final Gson gson = new Gson();
        Restaurant restaurant = gson.fromJson(Preferences.get().getString(Preferences.RESTAURANT), Restaurant.class);
        TextView deliverType = (TextView) root.findViewById(R.id.textView42);
        TextView min_sum = (TextView) root.findViewById(R.id.textView46);
        min_sum_to = (TextView) root.findViewById(R.id.textView35);
        sum = (TextView) root.findViewById(R.id.textView40);
        total = (TextView) root.findViewById(R.id.textView41);
        priceMin = restaurant.getPrice_min();
        min_sum.setText("" + priceMin);
        deliverType.setText(restaurant.isDelivery_free() ? "Бесплатно" : "Платно");
        list = (ListView) root.findViewById(R.id.listView2);
        final ArrayList<Dish> dishAdapter = new ArrayList<>();
        adapter = new ListAdapterDishBasket(getActivity(), R.layout.basket_item, dishAdapter, totalLayout, minSumLayout, priceMin, rlOrederLeft, textViewLight);
        list.setAdapter(adapter);
        String response = Preferences.get().getString(Preferences.dishes);
        DishObj dishObj = gson.fromJson(response, DishObj.class);
        if (dishObj != null) {
            Dish[] dishs = dishObj.getDish();
            if (dishs != null) {
                HashMap<Integer, Integer> hashMap = DatabaseHelper.getDishNotCountZero(getActivity());
                for (int i = 0; i < dishs.length; i++) {
                    if (hashMap.containsKey(dishs[i].getId()))
                        dishAdapter.add(dishs[i]);
                }
                adapter.notifyDataSetChanged();
            }
        }

            int sumBasket = DatabaseHelper.getSumBasket(getActivity());
            TextView sum = (TextView) totalLayout.findViewById(R.id.textView40);
            TextView total = (TextView) totalLayout.findViewById(R.id.textView41);
            sum.setText("" + sumBasket);
            total.setText("" + sumBasket);

            int howToNeed = sumBasket - priceMin;
            RelativeLayout rl_min_sum_order = (RelativeLayout) root.findViewById(R.id.rl_min_sum_order);
            if (howToNeed < 0) {
                rl_min_sum_order.setVisibility(View.VISIBLE);
                TextView minSum = (TextView) rl_min_sum_order.findViewById(R.id.textView35);
                minSum.setText("" + (-howToNeed) + " рублей");
            } else {
                rl_min_sum_order.setVisibility(View.GONE);
            }
        return root;
    }

}