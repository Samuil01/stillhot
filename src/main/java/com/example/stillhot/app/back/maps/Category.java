package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 11.10.2015.
 */
public class Category {
    @SerializedName("id")
    int id;

    @SerializedName("title")
    String title;

    @SerializedName("icon_app")
    String icon_app;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getIcon_app() {
        return icon_app;
    }
}
