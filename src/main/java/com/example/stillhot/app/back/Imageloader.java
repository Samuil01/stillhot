package com.example.stillhot.app.back;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;
import com.example.stillhot.app.back.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.example.stillhot.app.back.universalimageloader.core.DisplayImageOptions;
import com.example.stillhot.app.back.universalimageloader.core.ImageLoader;
import com.example.stillhot.app.back.universalimageloader.core.ImageLoaderConfiguration;
import com.example.stillhot.app.back.universalimageloader.core.assist.ImageScaleType;
import com.example.stillhot.app.back.universalimageloader.core.assist.QueueProcessingType;
import com.example.stillhot.app.back.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.example.stillhot.app.back.universalimageloader.core.display.RoundedBitmapDisplayer;
import com.example.stillhot.app.back.universalimageloader.core.listener.ImageLoadingListener;
import com.example.stillhot.app.back.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by dev on 04.06.2015.
 */
public class Imageloader {
    private static DisplayImageOptions options;
    private static ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();
    private static ImageLoader imageLoader = ImageLoader.getInstance();
    Drawable drawable;
    static ImageView.ScaleType scaleType = ImageView.ScaleType.CENTER_CROP;

    private static Imageloader imageloader;

    public static void init(Context context) {
        if (imageloader == null)
            imageloader = new Imageloader(context);
    }

    public static Imageloader get() {
        return imageloader;
    }

    public void setScaleType(ImageView.ScaleType scaleType) {
        this.scaleType = scaleType;
    }

    public Imageloader(Context context) { //, int type
//        switch (type) {
//            case 0:
//                drawable = context.getResources().getDrawable(R.drawable.placeholdersmall_x);
//                break;
//            case 1:
//                drawable = context.getResources().getDrawable(R.drawable.abc_ab_share_pack_mtrl_alpha);
//                break;
//            case 2:
//                drawable = context.getResources().getDrawable(R.drawable.abc_ab_share_pack_mtrl_alpha);
//                break;
//            case 3:
//                drawable = new ColorDrawable(1);
//        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
                .threadPriority(Thread.MIN_PRIORITY)
                .denyCacheImageMultipleSizesInMemory()
                .diskCacheFileNameGenerator(new Md5FileNameGenerator())
                .diskCacheSize(50 * 1024 * 1024) // 100 Mb
                .tasksProcessingOrder(QueueProcessingType.FIFO)
//                .writeDebugLogs() // Remove for release app
                .build();
        ImageLoader.getInstance().init(config);
    }

    public void loadImage(String url_image, ImageView image) {
        imageLoader.displayImage(url_image, image, options, animateFirstListener); //
    }

    public Imageloader setPlaceholder(Drawable drawable) {
        RoundedBitmapDisplayer rbd = new RoundedBitmapDisplayer(1);
        options = new DisplayImageOptions.Builder()
                .showImageOnLoading(drawable)  //R.drawable.ic_stub R.drawable.empty_photo
                .showImageForEmptyUri(drawable)
                .showImageOnFail(drawable)
                .cacheInMemory(true)
                .imageScaleType(ImageScaleType.EXACTLY) //IN_SAMPLE_INT
                .cacheOnDisk(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .considerExifParams(true)
                .displayer(rbd)
                .delayBeforeLoading(500)
                .build();
        return imageloader;
    }

    static class AnimateFirstDisplayListener extends SimpleImageLoadingListener {
        static final List<String> displayedImages = Collections.synchronizedList(new LinkedList<String>());

        @Override
        public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
            if (loadedImage != null) {
//                loadedImage.createScaledBitmap(loadedImage, 550, 250, true);
//                loadedImage.createBitmap(loadedImage);
                ImageView imageView = (ImageView) view;
                /*((ImageView) view).setScaleType(scaleType);*///CENTER_CROP
                imageView.setImageBitmap(loadedImage);
                boolean firstDisplay = !displayedImages.contains(imageUri);
                if (firstDisplay) {
                    FadeInBitmapDisplayer.animate(imageView, 600);
                    displayedImages.add(imageUri);

                }
            }
        }
    }
}