package com.example.stillhot.app.back.maps;

import com.example.stillhot.app.back.DateController;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 26.10.2015.
 */
public class Reviews {

    @SerializedName("author")
    String author;

    @SerializedName("date")
    String date;

    @SerializedName("message")
    String message;

    @SerializedName("rating")
    int rating;

    public String getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public int getRating() {
        return rating;
    }

    public String getTime() {
        return DateController.getMyDateWithFormat(date, "HH:mm");
    }
    public String getDate() {
        return DateController.getMyDateWithFormat(date, "dd MMMM");
    }
}