package com.example.stillhot.app.back.maps;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 19.10.2015.
 */
public class Dish {

    @SerializedName("id")
    int id;

    @SerializedName("cities")
    String city;

    @SerializedName("restaurants")
    int restaurants;

    @SerializedName("title")
    String title;

    @SerializedName("descr")
    String descr;

    @SerializedName("price")
    int price;

    @SerializedName("photo")
    String photo;

    @SerializedName("cat_title")
    String cat_title;

    @SerializedName("subcat_title")
    String subcat_title;


    public int getId() {
        return id;
    }

    public String getCity() {
        return city;
    }

    public int getRestaurants() {
        return restaurants;
    }

    public String getTitle() {
        return title;
    }

    public String getDescr() {
        return descr;
    }

    public int getPrice() {
        return price;
    }

    public String getPhoto() {
        return photo;
    }

    public String getCat_title() {
        return cat_title;
    }

    public String getSubcat_title() {
        return subcat_title;
    }
}
