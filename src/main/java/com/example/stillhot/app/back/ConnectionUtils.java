package com.example.stillhot.app.back;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public class ConnectionUtils {

	static public int CONNECT_TYPE_3G = 0;
	static public int CONNECT_TYPE_WIFI = 1;

	/**
	 * Check has internet connection
	 */
	static public boolean isOnline(Context c) {
		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		return netInfo != null && netInfo.isConnectedOrConnecting();
	}

	/**
	 * Get connection type
	 */
	static public int getType(Context c) {
		ConnectivityManager cm = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
		// mobile
		NetworkInfo.State mobile = cm.getNetworkInfo(0).getState();
		// wifi
		NetworkInfo.State wifi = cm.getNetworkInfo(1).getState();
		if (mobile == NetworkInfo.State.CONNECTED
				|| mobile == NetworkInfo.State.CONNECTING) {
			return CONNECT_TYPE_3G;
		} else if (wifi == NetworkInfo.State.CONNECTED
				|| wifi == NetworkInfo.State.CONNECTING) {
			return CONNECT_TYPE_WIFI;
		}
		return -1;
	}

	/**
	 * Get operator name
	 */
	static public String getOperatorName(Context c) {
		TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getNetworkOperatorName();
	}
}