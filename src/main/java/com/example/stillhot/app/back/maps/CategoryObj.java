package com.example.stillhot.app.back.maps;


import com.google.gson.annotations.SerializedName;

/**
 * Created by Admin on 11.10.2015.
 */
public class CategoryObj {
    @SerializedName("categories")
    Category[] categories;

    public Category[] getCategories() {
        return categories;
    }
}
