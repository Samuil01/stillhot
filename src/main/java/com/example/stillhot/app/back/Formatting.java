package com.example.stillhot.app.back;

/**
 * Created by Robot on 13.07.2015.
 */
public class Formatting {
    public static String formatCount(String[] words, Long count) {
        if ((count % 10 == 0) || (count % 10 > 4) || (count % 100 == 11)) {
            return count + " " + words[2];
        } else if (count % 10 == 1) {
            return count + " " + words[0];
        } else {
            return count + " " + words[1];
        }
    }
}
