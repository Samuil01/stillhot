package com.example.stillhot.app.back;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import com.example.stillhot.app.Activities.StartActivities;

/**
 * Created by Admin on 04.10.2015.
 */
public class MyLocationListener implements LocationListener {

    Context context;
    static LocationManager locationManager;

    public MyLocationListener(Context context) {
        this.context = context;
    }

    static Location imHere;
    static LocationListener locationListener;
    public static void SetUpLocationListener(Context context)
    {
        locationManager = (LocationManager)
                context.getSystemService(Context.LOCATION_SERVICE);

        locationListener = new MyLocationListener(context);

        locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                4000,
                10,
                locationListener);
        locationManager.requestLocationUpdates(
                LocationManager.NETWORK_PROVIDER,
                4000,
                10,
                locationListener);

        imHere = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    public void onLocationChanged(Location location) {
        imHere = location;
        Intent intent = new Intent(StartActivities.BROADCAST_ACTION);
        intent.putExtra("Latitude", location.getLatitude());
        intent.putExtra("Longitude", location.getLongitude());
        locationManager.removeUpdates(locationListener);
        context.sendBroadcast(intent);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {
        locationManager.getLastKnownLocation(provider);
    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
