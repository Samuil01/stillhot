package com.example.stillhot.app.back;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Created by Admin on 26.10.2015.
 */
public class DateController {
    public static String getMyDateWithFormat(String date_str, String format_str) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("UTC"));
        SimpleDateFormat formatOut = new SimpleDateFormat(format_str);  //"dd.MM.yy"
        Date lDate = new Date();
        try {
            lDate = format.parse(date_str);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
        return formatOut.format(lDate);
    }

    public static String getMyTimeWithFormat(String date_str, String format_str) {
        SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.ENGLISH);
        format.setTimeZone(TimeZone.getTimeZone("GMT"));
        SimpleDateFormat formatOut = new SimpleDateFormat(format_str);  //"dd.MM.yy"
        Date lDate = new Date();
        try {
            lDate = format.parse(date_str);
        } catch (ParseException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {

        }
        return formatOut.format(lDate);
    }
}
