package com.example.stillhot.app.back;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.text.TextUtils;

import java.util.Map;

/**
 * Created by dev on 25.08.2015.
 */
public class Preferences {

    public static final String SET_COOKIE_KEY = "Set-Cookie";
    public static final String COOKIE_KEY = "Cookie";
    public static final String IG = "IG";
    public static final String SESSION_COOKIE = "PHPSESSID";
    public static final String OTHER_SOCIAL_NETWORK = "Other_Social_Network";
    public static final String LAST_REQUEST = "last_request";
    public static final String REGISTRATION = "Registration";
    public static final String IF_MODIFIED_SINCE = "If-Modified-Since";
    public static final String CITY = "city";
    public static final String CITYLIST = "citylist";
    public static final String RUBRICOBJECT = "citylist";
    public static final String RESTAURANT = "restaurant";
    public static final String dishes = "dishes";

    private SharedPreferences _preferences;

    Context context;

    static Preferences preferences;
    SharedPreferences.Editor prefEditor;

    public Preferences(Context context) {
        this.context = context;
        _preferences = PreferenceManager.getDefaultSharedPreferences(context);
        prefEditor = _preferences.edit();
    }

    public static void init(Context context) {
        if (preferences == null)
            preferences = new Preferences(context);
    }

    public static Preferences get() {
        return preferences;
    }

    public final void checkSessionCookie(Map<String, String> headers) {
        if (headers.containsKey(SET_COOKIE_KEY)
                && headers.get(SET_COOKIE_KEY).startsWith(SESSION_COOKIE)) {
            String cookie = headers.get(SET_COOKIE_KEY);
            if (cookie.length() > 0) {
                String[] splitCookie = cookie.split(";");
                String[] splitSessionId = splitCookie[0].split("=");
                cookie = splitSessionId[1];
                prefEditor.putString(SESSION_COOKIE, cookie);
                prefEditor.commit();
            }
        }
    }

    /**
     * Adds session cookie to headers if exists.
     *
     * @param headers
     */
    public final void addSessionCookie(Map<String, String> headers) {
        String sessionId = _preferences.getString(SESSION_COOKIE, "");
        if (sessionId.length() > 0) {
            StringBuilder builder = new StringBuilder();
            builder.append(SESSION_COOKIE);
            builder.append("=");
            builder.append(sessionId);
            if (headers.containsKey(COOKIE_KEY)) {
                builder.append("; ");
                builder.append(headers.get(COOKIE_KEY));
            }
            headers.put(COOKIE_KEY, builder.toString());
        }
    }

    public String getCookie() {
        return _preferences.getString(SESSION_COOKIE, "");
    }

    public void eraseCookie() {
        prefEditor.putString(SESSION_COOKIE, "");
        prefEditor.commit();
    }

    public void eraseTocken() {
        prefEditor.putString(IG, "");
        prefEditor.commit();
    }

    public void eraseOther() {
        prefEditor.putString(OTHER_SOCIAL_NETWORK, "");
        prefEditor.commit();
    }

    public void eraseRegistration() {
        prefEditor.putString(REGISTRATION, "");
        prefEditor.commit();
    }

    public String getString(String key) {
        return _preferences.getString(key, "");
    }

    public void putString(String key, String s) {
        prefEditor.putString(key, s);
        prefEditor.commit();
    }

    public void putLong(String key, Long l) {
        prefEditor.putLong(key, l);
        prefEditor.commit();
    }

    public void putInt(String key, int i) {
        prefEditor.putInt(key, i);
        prefEditor.commit();
    }

    public long getLong(String key) {
        return _preferences.getLong(key, 0L);
    }

    public int getInt(String key) {
        return _preferences.getInt(key, 0);
    }

    public String getName() {
        String ig = getString(Preferences.IG);
        String myReg = getString(Preferences.REGISTRATION);
        String other = getString(OTHER_SOCIAL_NETWORK);
        if (!TextUtils.isEmpty(ig))
            return ig;
        else if (!TextUtils.isEmpty(myReg))
            return myReg;
        else if (!TextUtils.isEmpty(other))
            return other;
        else
            return null;
    }

    public void saveIMS(Map<String, String> headers) {
        if (headers.containsKey(IF_MODIFIED_SINCE))
            putString(IF_MODIFIED_SINCE, headers.get(IF_MODIFIED_SINCE));
    }
}