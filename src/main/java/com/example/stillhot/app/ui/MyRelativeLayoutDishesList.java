package com.example.stillhot.app.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

/**
 * Created by Admin on 05.11.2015.
 */
public class MyRelativeLayoutDishesList extends RelativeLayout {
    private ViewGroup viewGroup;
    private String key;
    private String keyInner;
    public MyRelativeLayoutDishesList(Context context) {
        super(context);
    }

    public MyRelativeLayoutDishesList(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setViewGroup(ViewGroup viewGroup){
        this.viewGroup = viewGroup;
    }

    public ViewGroup getViewGroup() {
        return viewGroup;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setKeyInner(String keyInner) {
        this.keyInner = keyInner;
    }

    public String getKey() {

        return key;
    }

    public String getKeyInner() {
        return keyInner;
    }
}
