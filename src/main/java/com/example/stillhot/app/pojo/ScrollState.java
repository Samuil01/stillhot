package com.example.stillhot.app.pojo;

/**
 * Created by Ruslan on 27.09.2015.
 */
public enum ScrollState {
    STOP,
    UP,
    DOWN;

    private ScrollState() {
    }
}