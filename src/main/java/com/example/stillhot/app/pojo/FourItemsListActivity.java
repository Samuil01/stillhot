package com.example.stillhot.app.pojo;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.*;
import com.example.stillhot.app.ListAdapter;
import com.example.stillhot.app.MainActivity;
import com.example.stillhot.app.back.*;
import com.example.stillhot.app.back.maps.Category;
import com.example.stillhot.app.back.maps.City;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Руслан on 29.09.2015.
 */
public class FourItemsListActivity extends Fragment {
    ListAdapter adapter;
    ListView list;
    android.widget.RelativeLayout relativeLayout;
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final ViewGroup root = (ViewGroup) inflater.inflate(R.layout.activity_four_items_list, null);
        MainActivity.currentFragment = "com.example.stillhot.app.pojo.FourItemsListActivity";
        MainActivity.setVisibilityForLogo();
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        progressBar = (ProgressBar) root.findViewById(R.id.progressBar);
        relativeLayout = (android.widget.RelativeLayout) root.findViewById(R.id.rl_bottom);
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int stHeight = getStatusBarHeight() + getToolbarHeight();
        final int height;
        float ht_px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics());
        height = metrics.heightPixels - stHeight - (int) ht_px;
        list = (ListView) root.findViewById(R.id.listView);
        final ArrayList<Rubrica> rubrics = new ArrayList<>();
        adapter = new ListAdapter(this, getActivity(), R.layout.four_list_item, rubrics, height);
        list.setAdapter(adapter);
        RelativeLayout basketFloor = (RelativeLayout) root.findViewById(R.id.rl_bottom);
        basketFloor.setVisibility(View.GONE);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Rubrica rubrica = rubrics.get(position);
                int[] categories = rubrica.getCategories();
                if (categories.length > 1) {
                    MainActivity.preFragment = "";
                    AppController.currentRubric = rubrica.getId();
                    MainActivity.fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.CategoryFragment"))
                            .commit();
                } else {
                    MainActivity.preFragment = "com.example.stillhot.app.pojo.FourItemsListActivity";
                    AppController.currentRubric = rubrica.getId();
                    AppController.currentCategory = categories[0];
                    MainActivity.fragmentManager.beginTransaction()
                            .replace(R.id.container, Fragment.instantiate(getActivity(), "com.example.stillhot.app.Fragments.RestaurantFragment"))
                            .commit();
                }
            }
        });
        final Gson gson = new Gson();
        City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.rubrics + "city=" + city.getId(),
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        RubricObject rubrica = gson.fromJson(response, RubricObject.class);
                        if (rubrica != null) {
                            Rubrica[] rubricas = rubrica.getRubricas();
                            if (rubricas != null) {
                                rubrics.addAll(Arrays.asList(rubricas));
                                Preferences.get().putString(Preferences.RUBRICOBJECT, response);
                                AppController.rubricCount = rubrics.size();
                                MainActivity.addToSlideBar(rubrics);
                                progressBar.setVisibility(View.GONE);
                                adapter.notifyDataSetChanged();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        Connection.getInstance(getActivity()).addToRequestQueue(stringRequest, FourItemsListActivity.class.getName());
        return root;
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    public int getToolbarHeight() {
        Resources resources = getActivity().getResources();
        int resourceId = getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            int i = resources.getDimensionPixelSize(resourceId);
            return i;
        }
        return 0;
    }

}
