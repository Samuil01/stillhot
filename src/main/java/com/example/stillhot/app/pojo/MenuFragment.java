package com.example.stillhot.app.pojo;

import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.stillhot.app.R;
import com.example.stillhot.app.back.*;
import com.example.stillhot.app.back.maps.City;
import com.example.stillhot.app.back.maps.Dish;
import com.example.stillhot.app.back.maps.DishObj;
import com.example.stillhot.app.ui.MyRelativeLayoutDishesList;
import com.example.stillhot.app.ui.MyTextViewBold;
import com.example.stillhot.app.ui.MyTextViewLight;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.*;

/**
 * Created by Руслан on 25.09.2015.
 */
public class MenuFragment extends BaseFragment {
    HashMap<String, HashMap<String, ArrayList<Dish>>> hashMap = new HashMap<>();
    public static final String ARG_SCROLL_Y = "ARG_SCROLL_Y";
    MyTextViewLight textViewLight;
    RelativeLayout basketFloor;
    HashMap<Integer, Integer> dishCount;
    RelativeLayout.LayoutParams im_params, count_par, plus_par, minus_par, zakaz_par;
    int px;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_scrollview, container, false);
        final LinearLayout root = (LinearLayout) view.findViewById(R.id.root_layout);

        ViewGroup dishLayout = (ViewGroup) inflater.inflate(R.layout.dish_item, null, false);

        im_params = (RelativeLayout.LayoutParams) dishLayout.findViewById(R.id.dish_image).getLayoutParams();
        count_par = (RelativeLayout.LayoutParams) dishLayout.findViewById(R.id.textView17).getLayoutParams();
        plus_par = (RelativeLayout.LayoutParams) dishLayout.findViewById(R.id.textView18).getLayoutParams();
        minus_par = (RelativeLayout.LayoutParams) dishLayout.findViewById(R.id.textView19).getLayoutParams();
        zakaz_par = (RelativeLayout.LayoutParams) dishLayout.findViewById(R.id.textView17_).getLayoutParams();

        /*final ViewGroup scrollList = (ViewGroup) inflater.inflate(R.layout.listviewscroll, null);*/
        final ViewGroup prorassView = (ViewGroup) inflater.inflate(R.layout.progresslayout, null);
        root.addView(prorassView, 1);
        /*root.addView(scrollList);*/
        /*final LinearLayout linearLayoutScroll = (LinearLayout) scrollList.findViewById(R.id.ll_scroll);*/
        basketFloor = (RelativeLayout) getParentFragment().getView().findViewById(R.id.test);
        Resources r = getActivity().getResources();
        px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                3,
                r.getDisplayMetrics());

        final Gson gson = new Gson();
        /* final ArrayList<Dish> dishArrayList = new ArrayList<>();
        final ListAdapterDish adapter = new ListAdapterDish(getActivity(), R.layout.dish_item, dishArrayList, basketFloor);
        ListView listView = (ListView) scrollList.findViewById(R.id.lv_for_scroll);
        listView.setAdapter(adapter);*/
        City city = gson.fromJson(Preferences.get().getString(Preferences.CITY), City.class);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, Addresses.restmenu
                + "city=" + city.getId()
                + "&rest=" + AppController.currentRestaurantID,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Preferences.get().putString(Preferences.dishes, response);
                        DishObj dishObj = gson.fromJson(response, DishObj.class);
                        if (dishObj != null) {
                            Dish[] dishs = dishObj.getDish();
                            if (dishs != null) {
                                for (int i = 0; i < dishs.length; i++) {
                                    if (hashMap.containsKey(dishs[i].getCat_title())) {
                                        HashMap<String, ArrayList<Dish>> subTitle = hashMap.get(dishs[i].getCat_title());
                                        if (subTitle.containsKey(dishs[i].getSubcat_title())) {
                                            ArrayList<Dish> arrayList = subTitle.get(dishs[i].getSubcat_title());
                                            arrayList.add(dishs[i]);
                                            subTitle.put(dishs[i].getSubcat_title(), arrayList);
                                        } else {
                                            ArrayList<Dish> arrayList = new ArrayList<>();
                                            arrayList.add(dishs[i]);
                                            subTitle.put(dishs[i].getSubcat_title(), arrayList);
                                        }
                                        hashMap.put(dishs[i].getCat_title(), subTitle);
                                    } else {
                                        HashMap<String, ArrayList<Dish>> subTitle = new HashMap<>();
                                        ArrayList<Dish> arrayList = new ArrayList<>();
                                        arrayList.add(dishs[i]);
                                        subTitle.put(dishs[i].getSubcat_title(), arrayList);
                                        hashMap.put(dishs[i].getCat_title(), subTitle);
                                    }
                                }
                                DatabaseHelper.addDishes(getActivity(), dishs);
                            }
                        }
                        dishCount = DatabaseHelper.getDishCount(getActivity());
                        root.removeViewAt(1);
                        for (Map.Entry<String, HashMap<String, ArrayList<Dish>>> entry : hashMap.entrySet()) {
                            MyRelativeLayoutDishesList categorySpinner = addCategorySpinner(root, entry.getKey(), true);
                            categorySpinner.setKey(entry.getKey());
                            ImageView arrow = (ImageView) categorySpinner.findViewById(R.id.spinner_arrow);
                            arrow.setTag(false);
                            setRotateClickListenerForMain(arrow, categorySpinner);
                            addDevider(root);
                        }
                        checkBasket();
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        Connection.getInstance(getActivity().getApplicationContext()).addToRequestQueue(stringRequest, MenuFragment.class.getName());
        for (int i = 0; i < 4; i++) {

        }

        final ObservableScrollView scrollView = (ObservableScrollView) view.findViewById(R.id.scroll);
        Fragment parentActivity = getParentFragment();
        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            // Scroll to the specified offset after layout
            Bundle args = getArguments();
            if (args != null && args.containsKey(ARG_SCROLL_Y)) {
                final int scrollY = args.getInt(ARG_SCROLL_Y, 0);
                ScrollUtils.addOnGlobalLayoutListener(scrollView, new Runnable() {
                    @Override
                    public void run() {
                        scrollView.scrollTo(0, scrollY);
                    }
                });
            }

            // TouchInterceptionViewGroup should be a parent view other than ViewPager.
            // This is a workaround for the issue #117:
            // https://github.com/ksoichiro/Android-ObservableScrollView/issues/117
            scrollView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.getView().findViewById(R.id.root));
            scrollView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }
        return view; //scrollView
    }

    /*private void addWhiteDevider(LinearLayout lay) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                px);
        View deviderLine = new View(getActivity());
        deviderLine.setLayoutParams(params);
        deviderLine.setBackgroundColor(0xFFFFFFFF);
        lay.addView(deviderLine);
    }*/

    private void addDevider(ViewGroup lay) {
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, 2);
        View deviderLine = new View(getActivity());
        deviderLine.setLayoutParams(params);
        deviderLine.setBackgroundColor(0xFFe3e3e3);
        lay.addView(deviderLine);
    }

    private MyRelativeLayoutDishesList addCategorySpinner(ViewGroup lay, String name, boolean isMain) { //добавить спиннер в linearlayout lay с заголовком name
        MyRelativeLayoutDishesList spinner = (MyRelativeLayoutDishesList) View.inflate(getActivity(), R.layout.spinner_layout, null);
        TextView category = (TextView) spinner.findViewById(R.id.name);
        if (!isMain) {
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) category.getLayoutParams();
            layoutParams.setMargins(px, 0, px, 0);
            category.setLayoutParams(layoutParams);
            category.setBackgroundColor(0xFFFFFFFF);
            category.setTextSize(14f);
            category.setText("    " + name);
        } else {
            spinner.setTag("sticky");
            category.setText(name);
        }
        LinearLayout content = new LinearLayout(getActivity());
        content.setOrientation(LinearLayout.VERTICAL);
        content.setVisibility(View.GONE);
        lay.addView(spinner);
        spinner.setViewGroup(content);
        lay.addView(content);
        return spinner;
    }

    private void addDish(final ViewGroup lay, final Dish dish) {
        LayoutInflater inflater = (LayoutInflater) getActivity()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        ViewGroup dishLayout = (ViewGroup) inflater.inflate(R.layout.dish_item_temp, null, false);
        TextView name = (TextView) dishLayout.findViewById(R.id.name);
        name.setText(dish.getTitle());
        TextView desc = (TextView) dishLayout.findViewById(R.id.textView16);
        desc.setText(dish.getDescr());
        TextView tv_price = (TextView) dishLayout.findViewById(R.id.textView7);
        tv_price.setText(String.valueOf(dish.getPrice()));
        final TextView zakaz = (TextView) dishLayout.findViewById(R.id.textView17_);
        ImageView image = (ImageView) dishLayout.findViewById(R.id.dish_image);
        /*if (!TextUtils.isEmpty(dish.getPhoto()))
            Picasso.with(getActivity()).load(dish.getPhoto()).into(image);*/
        Imageloader.get().loadImage(dish.getPhoto(), image);
        final TextView tv_count = (TextView) dishLayout.findViewById(R.id.textView17);
        int count = dishCount.get(dish.getId());
        if (count != 0) {
            tv_count.setVisibility(View.VISIBLE);
            zakaz.setVisibility(View.INVISIBLE);
            tv_count.setText("" + count);
        } else {
            //tv_count.setVisibility(View.INVISIBLE);
            zakaz.setVisibility(View.VISIBLE);
            //tv_count.setText("0");
        }

        final View tv_minus = (View) dishLayout.findViewById(R.id.textView19);
        tv_minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = DatabaseHelper.deductDishCount(getActivity(), dish.getId());
                if (n <= 0) {
                    checkBasket();
                    tv_minus.setEnabled(false);
                    tv_count.setText("");
                    tv_count.setVisibility(View.INVISIBLE);
                    zakaz.setVisibility(View.VISIBLE);
                } else {
                    tv_count.setText("" + n);
                    tv_count.setVisibility(View.VISIBLE);
                }
                checkBasket();
            }
        });

        zakaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = DatabaseHelper.addDishCount(getActivity(), dish.getId());
                tv_count.setText("" + n);
                tv_count.setVisibility(View.VISIBLE);
                tv_minus.setEnabled(true);
                zakaz.setVisibility(View.GONE);
                checkBasket();
            }
        });

        View tv_plus = (View) dishLayout.findViewById(R.id.textView18);
        tv_plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int n = DatabaseHelper.addDishCount(getActivity(), dish.getId());
                tv_count.setText("" + n);
                tv_count.setVisibility(View.VISIBLE);
                tv_minus.setEnabled(true);
                checkBasket();
            }
        });
        lay.addView(dishLayout);
    }

    private synchronized void checkBasket() {
        int count = DatabaseHelper.checkBasketCount(getActivity());
        TextView tv_basketCount = (MyTextViewBold) basketFloor.findViewById(R.id.textView15);
        tv_basketCount.setText("" + count);
        basketFloor.setVisibility((count <= 0) ? View.GONE : View.VISIBLE);
    }

    private void setRotateClickListener(final ImageView arrow, final MyRelativeLayoutDishesList categorySpinnerInner) { //при нажатии на v, показывается show
        textViewLight = (MyTextViewLight) categorySpinnerInner.findViewById(R.id.name);
        textViewLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((boolean) arrow.getTag()) {
                    arrow.animate().rotation(0).setDuration(0);
                    categorySpinnerInner.getViewGroup().setVisibility(View.GONE);
                } else {
                    arrow.animate().rotation(180).setDuration(0);
                    if (categorySpinnerInner.getViewGroup().getChildCount() == 0) {
                        ArrayList<Dish> arrayDishes = hashMap.get(categorySpinnerInner.getKey()).get(categorySpinnerInner.getKeyInner());
                        for (int i = 0; i < arrayDishes.size(); i++) {
                            addDevider(categorySpinnerInner.getViewGroup());
                            addDish(categorySpinnerInner.getViewGroup(), arrayDishes.get(i));
                        }
                    }
                    categorySpinnerInner.getViewGroup().setVisibility(View.VISIBLE);
                }
                arrow.setTag(!(boolean) arrow.getTag());
            }
        });
    }

    private void setRotateClickListenerForMain(final ImageView arrow, final MyRelativeLayoutDishesList categorySpinner) { //при нажатии на v, показывается show
        textViewLight = (MyTextViewLight) categorySpinner.findViewById(R.id.name);
        textViewLight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((boolean) arrow.getTag()) {
                    arrow.animate().rotation(0).setDuration(0);
                    categorySpinner.getViewGroup().setVisibility(View.GONE);
                } else {
                    arrow.animate().rotation(180).setDuration(0);
                    if (categorySpinner.getViewGroup().getChildCount() == 0) {
                        HashMap<String, ArrayList<Dish>> subTitles = hashMap.get(categorySpinner.getKey());
                        if (subTitles != null) {
                            for (Map.Entry<String, ArrayList<Dish>> subEntry : subTitles.entrySet()) {
                                boolean flag = false;
                                if (subEntry.getKey() != null) {
                                    flag = true;
                                    addDevider(categorySpinner.getViewGroup());
                                    MyRelativeLayoutDishesList categorySpinnerInner = addCategorySpinner(categorySpinner.getViewGroup(), subEntry.getKey(), false);
                                    categorySpinnerInner.setKey(categorySpinner.getKey());
                                    categorySpinnerInner.setKeyInner(subEntry.getKey());
                                    ImageView arrow = (ImageView) categorySpinnerInner.findViewById(R.id.spinner_arrow);
                                    arrow.setTag(false);
                                    setRotateClickListener(arrow, categorySpinnerInner);
                                }
                                if (!flag) {
                                    ArrayList<Dish> arrayDishes = subEntry.getValue();
                                    for (int i = 0; i < arrayDishes.size(); i++) {
                                        addDevider(categorySpinner.getViewGroup());
                                        addDish(categorySpinner.getViewGroup(), arrayDishes.get(i));
                                    }
                                } else {

                                }
                            }
                        }
                    }
                    categorySpinner.getViewGroup().setVisibility(View.VISIBLE);
                }
                arrow.setTag(!(boolean) arrow.getTag());
            }
        });
    }
}