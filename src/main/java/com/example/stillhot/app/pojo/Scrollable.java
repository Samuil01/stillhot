package com.example.stillhot.app.pojo;

import android.view.ViewGroup;


/**
 * Created by Ruslan on 27.09.2015.
 */
public interface Scrollable {
    void setScrollViewCallbacks(ObservableScrollViewCallbacks var1);

    void scrollVerticallyTo(int var1);

    int getCurrentScrollY();

    void setTouchInterceptionViewGroup(ViewGroup var1);
}